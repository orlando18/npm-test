import React from "react";
import { IconButton } from "../lib";
import { actions } from "@storybook/addon-actions";
import { withDesign } from "storybook-addon-designs";

export default {
  title: "Components/IconButton",
  component: IconButton,
  argTypes: {
    onClick: { action: "clicked" },
  },
  decorators: [withDesign],
};

const Template = (args) => <IconButton {...args} />;

export const Selected = Template.bind({});
Selected.args = {
  text: "USDC Stellar Instant Deposit",
  selected: true,
};

Selected.parameters = {
  design: {
    type: "figma",
    url:
      "https://www.figma.com/file/33wxouFYrvCavj0Z0KDuGe/2_Global-Components?node-id=1087%3A540",
  },
};

export const Unselected = Template.bind({});
Unselected.args = {
  text: "Wire Transfer",
  selected: false,
};

Unselected.parameters = {
  design: {
    type: "figma",
    url:
      "https://www.figma.com/file/33wxouFYrvCavj0Z0KDuGe/2_Global-Components?node-id=1087%3A542",
  },
};

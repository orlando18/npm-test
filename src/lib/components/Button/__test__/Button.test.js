import { render, screen } from "@testing-library/react";
import Button from "../Button";

test("renders button", () => {
  render(<Button label="Test" />);
  const linkElement = screen.queryByText(/test/i);
  expect(linkElement).not.toBeNull();
});

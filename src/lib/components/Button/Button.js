import React from "react";
import styles from "./Button.module.scss";

const Button = ({ variant, onClick, label }) => {
  return (
    <button data-variant={variant} className={styles.button} onClick={onClick}>
      {label}
    </button>
  );
};

export default Button;

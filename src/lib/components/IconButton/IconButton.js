import React from "react";
import styles from "./IconButton.module.scss";

const IconButton = ({ text, selected, onClick }) => {
  const handleOnClick = (event) => {
    onClick({
      event,
      additionalData: {
        message: "Button clicked",
      },
    });
  };

  return (
    <button
      data-state={selected ? "selected" : "unselected"}
      className={styles.iconButton}
      onClick={handleOnClick}
    >
      {text}
    </button>
  );
};

export default IconButton;

import { render, screen, fireEvent } from "@testing-library/react";
import IconButton from "../IconButton";

describe("IconButton", () => {
  it("should render properly", () => {
    render(<IconButton text="Test" />);
    const element = screen.queryByText(/test/i);
    expect(element).not.toBeNull();
  });

  it("should call onClick callback any time is pressed", () => {
    const handleClick = jest.fn();

    render(<IconButton text="Test" onClick={handleClick} />);
    const element = screen.queryByText(/test/i);
    fireEvent.click(element);
    expect(handleClick).toHaveBeenCalledTimes(1);
  });
});

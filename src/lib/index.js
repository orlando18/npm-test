import "./assets/fonts/PublicSans.ttf";

import Button from "./components/Button/Button";
import Badge from "./components/Badge/Badge";
import IconButton from "./components/IconButton/IconButton";
import DemoButton from "./components/DemoButton/DemoButton";

export { Button, Badge, IconButton, DemoButton };

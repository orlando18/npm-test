import React from "react";
import { Button, Badge } from "./lib";

function App() {
  return (
    <div>
      <h1>Npm Test</h1>
      <Button />
      <Badge />
    </div>
  );
}

export default App;
